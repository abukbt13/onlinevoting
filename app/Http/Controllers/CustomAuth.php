<?php

namespace App\Http\Controllers;

use App\Models\Voter;
use Illuminate\Http\Request;
use Hash;
use Session;
class CustomAuth extends Controller
{
    public function login(){
        return view('users.login');
    }

        public function loginuser(Request $request){
            $request->validate([            
                
                'uniqueid' => 'required',
                'secretkey'=>'required',
            ]);
          
           
            $user=Voter::where(['uniqueid' => $request->uniqueid])->first();
        
            if($user){
                if($request->uniqueid==$user->uniqueid){
                    
                    $request->session()->put('loginId',$user->id);
                    return redirect ('/vote');
                    
                }
                else{
                    return back()->with('fail','your credentials do not match ');
                }
            }
            else{
                return back()->with('fail','Registration number is not valid');
            }
        }
    
    
}
