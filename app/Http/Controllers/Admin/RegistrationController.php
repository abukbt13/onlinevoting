<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Session;
use App\Models\Voter;
use App\Models\Voting;
use App\Models\Aspirant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function registeraspirants(){
        return view('admin.aspirants');
    }


     public function registervoters(){
        return view('admin.voters');
    }
    public function dashboard(){
        
     
            $voters=Voter::count();
            $aspirants=Aspirant::count();
            $toppresident=Voting::where('president','President')->count();
            // $topgovernor=Voter::where('')
            return view('admin.dashboard.index',compact('voters','aspirants','toppresident'));
        
        
    }


    public function register_voter(Request $request){
             
        $request->validate([
       
            'votersfullnames' =>  'required|string',
            'age' => 'required|integer',
            'county' =>'required|string',
            'ward' => ':users',
            'constituency' => 'required|string',
            'secretkey' => 'required|string',
            'uniqueid' => 'required|string|unique:voters',
            
           
        ]);
        $voter=new Voter();
        
        $voter->votersfullnames = $request->votersfullnames;
        $voter->age = $request->age;
        $voter->county = $request->county;
       
        // $regid=$request->secretkey;
        
        $voter->secretkey = Hash::make($request->secretkey);
        $voter->ward = $request->ward;
        $voter->constituency = $request->constituency;
        $voter->constituency = $request->constituency;
        $voter->uniqueid = $request->uniqueid;

        $result=$voter->save();
            if($result){
                return redirect()->back()->with('message','The voter has been registered successfully');
            }
       

     
    }

    public function register_aspirant(Request $request){
        $request->validate([
       
            'aspiringfullnames' =>  'required|string',
            'aspiringposition' =>  'required|string',
            'age' => 'required|integer',
            'county' =>'required|string',
            'ward' => ':users',
            'constituency' => 'required|string',
            
           
        ]);
        $aspirant=new Aspirant();
        
        $aspirant->aspiringfullnames = $request->aspiringfullnames;
        $aspirant->age = $request->age;
        $aspirant->aspiringposition = $request->aspiringposition;
        
        
        
        $aspirant->county = $request->county;
        $aspirant->ward = $request->ward;
        $aspirant->constituency = $request->constituency;
        $aspirant->constituency = $request->constituency;
        $aspirant->uniqueid = $request->uniqueid;

        $result=$aspirant->save();
            if($result){
                return redirect()->back()->with('message','The Aspirant has been registered successfully');
            }
       
        
    }
        public function governor(Request $request){
        
            $county =Aspirant::where('county', $request->county)->where('aspiringposition','Governor')->get();
           
            return response()->json($county);
            }

        public function senator(Request $request){
        
            $county =Aspirant::where('county', $request->county)->where('aspiringposition','senator')->get();
           
            return response()->json($county);
            } 

            public function womenrep(Request $request){
        
            $county =Aspirant::where('county', $request->county)->where('aspiringposition','womenrep')->get();
           
            return response()->json($county);
            }
            public function mp(Request $request){
        
            $mp =Aspirant::where('constituency', $request->mp)->where('aspiringposition','mp')->get();
           
            return response()->json($mp);
            }

            public function mca(Request $request){
        
            $mca =Aspirant::where('ward', $request->ward)->where('aspiringposition','mca')->get();
           
            return response()->json($mca);
            }


public function vote(Request $request){
    $request->validate([
       
        'president' =>  'required|string',
        'governor' =>  'required|string',
        'senator' => 'required|string',
        'womenrep' =>'required|string',        
        'mp' =>'required|string',        
        'mca' =>'required|string',        
        // 'uid' =>'required',        
        
        
       
    ]);
     $vote=new Voting();
    $vote->president = $request->president;
    $vote->governor = $request->governor;
    $vote->senator = $request->senator;
    $vote->womenrep = $request->womenrep;
    // $vote->uid = $request->$request->uid;
    $vote->mp = $request->mp;
    $vote->mca = $request->mca;

    $res=$vote->save();
    if($res){
        return back()->with('message','You have successfully voted for your able leaders');
    }
    else{
        return back()->with('message','You can not vote twice');

    }

}


        }
