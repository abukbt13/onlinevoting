<?php

namespace App\Http\Controllers;

use App\Models\Aspirant;
use Illuminate\Http\Request;

class HomeController extends Controller
{
   
    public function home(){
        return view('home');
    } 
    public function vote(){
        $presidents=Aspirant::where('aspiringposition','President')->get();
    //    dd($presidents);
        return view('users.vote',compact('presidents'));
    }
  
      
    
   
   
}
