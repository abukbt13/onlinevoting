<?php

use App\Http\Controllers\CustomAuth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Admin\RegistrationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[HomeController::class,'home']);
Route::get('/vote',[HomeController::class,'vote']);


Route::get('/login',[CustomAuth::class,'login']);
Route::post('/login',[CustomAuth::class,'loginuser']);


Route::get('/aspirants',[RegistrationController::class,'registeraspirants']);
Route::get('/voters',[RegistrationController::class,'registervoters']);
Route::get('/dashboard',[RegistrationController::class,'dashboard']);

// register voter and aspirants
Route::post('/register_voter',[RegistrationController::class,'register_voter']);
Route::post('/register_aspirant',[RegistrationController::class,'register_aspirant']);


//fetch users from county level
Route::post('/governor',[RegistrationController::class,'governor']);
Route::post('/senator',[RegistrationController::class,'senator']);
Route::post('/womenrep',[RegistrationController::class,'womenrep']);

//fetch users from constituency level
Route::post('/mp',[RegistrationController::class,'mp']);

//fetch users from ward level
Route::post('/mp',[RegistrationController::class,'mp']);
Route::post('/mca',[RegistrationController::class,'mca']);


//do the voting
Route::post('/vote',[RegistrationController::class,'vote']);





Route::get('/logout',[CustomAuth::class,'logout']);