@extends('master')
@extends('layouts.navigation.header_navbar')


@section('title','voters')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-3 sidenav mt-2 ms-2">
          <p class="title text-center mt-2">IEBC</p>  
        
        <ul>
            <li>
                <a href="">Aspirants</a>
            </li>
                <li>
                <a href="">voters</a>
                </li>
            
        </ul>
        <ul>
            <li>
                <a href="{{url('aspirants')}}">Register Aspirants</a>
            </li>
            <li>
            <a href="{{url('voters')}}">Register Voters</a>
            </li>
        </ul>

        </div>


        <div class="col-sm-7 mt-3 ms-5 contents ">
        
                 <div class="row d-flex justify-content-sm-center">
                  <div class="col-sm-6">   
                    <form action="{{url('register_voter')}}" method="post">
                        <p class="text-primary text-center">Voter Registration</p>
                        @if(Session::has('message'))
                        <div class="alert alert-success">{{Session::get('message')}}</div>
                        @endif 
                        @if(Session::has('fail'))
                        <div class="alert alert-success">{{Session::get('fail')}}</div>
                        @endif


                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                <div>{{$error}}</div>
                                @endforeach

                            </div>
                            @endif
                        
                        @csrf
                    <div class="">
                        <label for="exampleInputEmail1" class="form-label">Voters Full names</label>
                        <input type="text" name="votersfullnames" value="{{old('votersfullnames')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="text text-danger">@error('votersfullnames'){{$message}}@enderror</span>

                    </div>  
                    <div class="">
                        <label for="" class="form-label">Age</label>
                        <input type="number" name="age" value="{{old('age')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="text text-danger">@error('age'){{$message}}@enderror</span>

                    </div>  
      


                    <div class="">
                        <label for="" class="form-label">County</label>
                        <select name="county" id="" value="{{old('county')}}" class="form-control">
                            <option value=>--select option--</option>
                            <option value="Elgeiyo marakwet">Elgeiyo marakwet</option>                                        
                            <option value="Kericho">Kericho</option>                                        
                            <option value="Bomet">Bomet</option>                                        
                            <option value="kwale">kwale</option>                                        
                            <option value="Uasin Ngishu">Uasin Ngishu</option>                                        
                        </select>

                    </div>  
                    
                    
                    <div class="">
                        <label for="" class="form-label">Constituency</label>
                       <select name="constituency" id="" class="form-control">
                            <option value="{{old('constituency')}}" >--select option--</option>
                            <option value="John">Keiyo south</option>                                       
                            <option value="John">Keiyo North</option>                                       
                            <option value="John">Marakwet west</option>                                       
                            <option value="John">Marakwet East</option>                                       
                        </select>

                    </div>


                
                    <div class="">
                        <label for="" class="form-label">Ward</label>
                        <select name="ward" id="" class="form-control">
                            <option value="{{old('ward')}}" >--select option--</option>
                            <option value="John">Kapsowar</option>   
                            <option value="John">Moibenkuserwo</option>   
                            <option value="John">Chesongoch</option>   
                            <option value="John">Uswo</option>   
                                
                                                               
                        </select>

                    </div> 


                    <div class="">
                        <label for="" class="form-label">National Id</label>
                        <input type="number" name="uniqueid" value="{{old('uniqueid')}}"  class="form-control" id="">
                        <span class="text text-danger">@error('uniqueid'){{$message}}@enderror</span>
                    </div>
                     <div class="">
                        <label for="" class="form-label">Secret key</label>
                        <input type="text" name="secretkey" value="{{old('secretkey')}}"  class="form-control" id="">
                        <span class="text text-danger">@error('secretkey'){{$message}}@enderror</span>
                    </div> 
                        
                    <button type="submit"  class="btn btn-primary mt-2 ">Register Voter</button>
                    </form>
                </div>    
              </div>
        </div>


    </div>
</div>
@endsection
