@extends('master')
@extends('layouts.navigation.header_navbar')


@section('title','dashboard')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-3 sidenav mt-2 ms-2">
          <p class="title text-center mt-2">IEBC</p>  
        
        <ul>
            <li>
                <a href="">Aspirants</a>
            </li>
                <li>
                <a href="">voters</a>
                </li>
            
        </ul>
        <ul>
            <li>
                <a href="{{url('aspirants')}}">Register Aspirants</a>
            </li>
            <li>
            <a href="{{url('voters')}}">Register Voters</a>
            </li>
        </ul>

        </div>


        <div class="col-sm-7 mt-3 contents-77">

        <div class="users view">
            <p>Voters</p>
            <h2>{{$voters}}</h2>
        </div>
        <div class="voters view">
            <p>Aspirants</p>
            <h2>{{$aspirants}}</h2>
        </div>
        <div class="aspirants view">
            <p></p>
            <h2></h2>
        </div>
        <div class="toppresident view">
            <h2></h2>
            <p>Top president</p>
        </div>
        <div class="topgovernor view">
            <p></p>
         <h2></h2> 
        </div>
        </div>


    </div>
</div>

@endsection