@extends('master')
@extends('layouts.navigation.header_navbar')


@section('title','aspirants')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-3 sidenav mt-2 ms-2">
          <p class="title text-center mt-2">IEBC</p>  
        
        <ul>
            <li>
                <a href="">Aspirants</a>
            </li>
                <li>
                <a href="">voters</a>
                </li>
            
        </ul>
        <ul>
            <li>
                <a href="{{url('aspirants')}}">Register Aspirants</a>
            </li>
            <li>
            <a href="{{url('voters')}}">Register Voters</a>
            </li>
        </ul>

        </div>


        <div class="col-sm-7 mt-3 ms-5 contents ">
        
                 <div class="row d-flex justify-content-sm-center">
                  <div class="col-sm-6">   
                    <form action="{{url('register_aspirant')}}" method="post">
                    @if(Session::has('message'))
                        <div class="alert alert-success">{{Session::get('message')}}</div>
                        @endif 
                        @if(Session::has('fail'))
                        <div class="alert alert-success">{{Session::get('fail')}}</div>
                        @endif


                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                <div>{{$error}}</div>
                                @endforeach

                            </div>
                            @endif
                        
                        @csrf
                    <div class="">
                        <label for="exampleInputEmail1" class="form-label">Aspirant Full names</label>
                        <input type="text" name="aspiringfullnames" value="{{old('aspiringfullnames')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="text text-danger">@error('aspiringfullnames'){{$message}}@enderror</span>

                    </div>  
                    <div class="">
                        <label for="" class="form-label">Age</label>
                        <input type="number" name="age" value="{{old('age')}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <span class="text text-danger">@error('age'){{$message}}@enderror</span>

                    </div>  
                    <div class="">
                        <label for="" class="form-label">Aspirant Position</label>
                        <select name="aspiringposition" id="" class="form-control">
                            <option value="">--select option--</option>
                            <option value="President">President</option>                                        
                            <option value="Governor">Governor</option>                                        
                            <option value="Senator">Senator</option>                                        
                            <option value="Women Rep">Women Rep</option>                                        
                            <option value="Member of  parliament">Member of  parliament</option>                                        
                            <option value="MCA">MCA</option>                                        
                        </select>

                    </div> 


                    <div class="">
                        <label for="" class="form-label">County</label>
                        <select name="county" id="" class="form-control">
                            <option value="">--select option--</option>
                            <option value="Elgeiyo marakwet">Elgeiyo marakwet</option>                                        
                            <option value="Kericho">Kericho</option>                                        
                            <option value="Nandi">Nandi</option>                                        
                            <option value="Bomet">Bomet</option>     Bukaywa                                   
                            <option value="kwale">kwale</option>                                        
                            <option value="Busia">Busia</option>                                        
                            <option value="Uasin Ngishu">Uasin Ngishu</option>                                        
                        </select>

                    </div>  
                    
                    
                    <div class="">
                        <label for="" class="form-label">Constituency</label>
                       <select name="constituency" id="" class="form-control">
                            <option value="">--select option--</option>
                            <option value="Kapsabet">Kapsabet</option>                                       
                            <option value="Malindi">Malindi </option>                                       
                            <option value="marakwet west">Marakwet west</option>                                       
                            <option value="Busia">Busia</option>                                       
                        </select>

                    </div>


                
                    <div class="">
                        <label for="" class="form-label">Ward</label>
                        <select name="ward" id="" class="form-control">
                            <option value="">--select option--</option>
                            <option value="Bukaywa">Bukaywa</option>   
                            <option value="Moibenkuserwo">Moibenkuserwo</option>   
                            <option value="Iten">Iten</option>   
                            <option value="Kapsowar">Kapsowar</option>   
                                
                                                               
                        </select>

                    </div> 


                    <div class="">
                        <label for="" class="form-label">National Id</label>
                        <input type="number" name="uniqueid" class="form-control" id="">
                        <span class="text text-danger">@error('uniqueid'){{$message}}@enderror</span>
                    </div> 
                        
                    <button type="submit"  class="btn btn-primary mt-2 ">Register Aspirant</button>
                    </form>
                </div>    
              </div>
        </div>


    </div>
</div>
@endsection
