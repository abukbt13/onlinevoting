<nav class="navbar navbar-expand-lg bg-light">
  <div class="container-fluid">
  <a class="navbar-brand" >IEBC ONLINE</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">

        <li class="nav-item">
          <a class="nav-link " aria-current="page" href="{{url('/')}}">Home</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="{{url('/login')}}">Login</a>
        </li>
 
         <li class="nav-item">
          <a class="nav-link" href="{{url('/dashboard')}}">Dashboard</a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="{{url('/vote')}}">Vote</a>
        </li>
     
     
      </ul>
        
        <div class="dropdown d-flex">
        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
          you
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" href="#">Logout</a></li>
            <li><a class="dropdown-item" href="#">Log in</a></li>
        </ul>
</div>
    </div>
  </div>
</nav>