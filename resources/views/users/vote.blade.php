@extends('master')
@extends('layouts.navigation.header_navbar')


@section('title','vote for your leader')

@section('content')

<div class="container">


        
            <div class="row vote-row d-flex justify-content-sm-center">
                <div class="col-sm-6">   
                    <form action="{{url('vote')}}" method="post">
                    @if(Session::has('message'))
                        <div class="alert alert-success">{{Session::get('message')}}</div>
                        @endif 
                        @if(Session::has('fail'))
                        <div class="alert alert-success">{{Session::get('fail')}}</div>
                        @endif


                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                <div>{{$error}}</div>
                                @endforeach

                            </div>
                            @endif
                        
                        @csrf
                       
                                    <div class="">
                        <label for="" class="form-label">Choose your President</label>
                        <select name="president" name="president" id="president" class="form-control">
                            <option value="">--select option--</option>
                            @foreach($presidents as $president)
                            <option value="{{$president->aspiringfullnames}}">{{$president->aspiringfullnames}}</option> 
                            @endforeach               
                        </select>

                    </div> 
                   
                    <h4>County Level</h4>
                    <p>Enter Your county to vote for your able leaders</p>
                    <select  name="county" id="county" class="form-control">
                            <option value="">--select option--</option> 
                            <option value="Elgeiyo marakwet">Elgeiyo marakwet</option>                                                                 
                            <option value="Nandi">Nandi</option>                                                                 
                            <option value="Baringo">Baringo</option>                                                                 
                    </select>

                    <div class="">
                        <label for="" class="form-label">Choose your Governor</label>
                        <select name="governor" id="governor" class="form-control">
                                        <option value="">--Select your county first--</option>                                                       
                        </select>
                    </div>
                    <div class="">
                        <label for="" class="form-label">Choose your senator</label>
                        <select name="senator" id="senator" class="form-control">
                            <option value="">--Select your Senator first--</option>                                                       
                        </select>
                    </div> 
                     <div class="">
                        <label for="" class="form-label">Choose your womenrep</label>
                        <select name="womenrep" id="womenrep" class="form-control">
                            <option value="">--Select your Senator first--</option>                                                       
                        </select>
                    </div>
                    <h4>Constituency level</h4>
                    <p>Enter your constituency to choose your able leaders</p>
                    <select  name="constituency" id="constituency" class="form-control">
                         <option value="">--Select your mp first--</option> 
                             <option value="marakwet west">marakwet west</option> 
                            <option value="keiyo south">keiyo south</option> 
                                                                                           
                              <!-- <option value="keiyo north">keiyo north</option>                                                               -->
                              <!-- <option value="marakwet east">marakwet east</option>                                                               -->
                    </select>

                    <div class="">
                        <label for="" class="form-label">Choose your mp</label>
                        <select name="mp" id="mp" class="form-control">
                            <option value="">--Select your mp first--</option>                                                       
                        </select>
                    </div>


                       <h4>Ward level</h4>
                       <p>Enter your ward to choose your respective wards</p>
                    <select  name="ward" id="ward" class="form-control">
                         <option value="">--Select your ward first--</option> 
                            
                            <option value="kapsowar">kapsowar</option> 
                            <option value="kapsumai">kapsumai</option> 
                                                                                           
                              <!-- <option value="keiyo north">keiyo north</option>                                                               -->
                              <!-- <option value="marakwet east">marakwet east</option>                                                               -->
                    </select>
                    <!-- <input type="text" value="voter" name="uid"> -->
                    <div class="">
                        <label for="" class="form-label">Choose your mca</label>
                        <select name="mca" id="mca" class="form-control">
                            <option value="">--Select your mp first--</option>                                                       
                        </select>
                    </div>


                       
                    <button type="submit"  class="btn btn-primary mt-2 ">Register Aspirant</button>
                    </form>
                </div> 
            </div>
</div>
@endsection
@section('script')

<script>
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

    $(document).ready(function () {
        $('#county').change(function (e) { 
            e.preventDefault();
            var county=$(this).val();
      style="display:none;"      
            $('#governor').html('');
           
           
          
            $.ajax({
                        type: "POST",
                        url: "/governor",
                        
                        data:{
                            'county': county,
                          
                        },
                        datatype: "json",
                        success:function(result) {
                           
                            console.log(result);

                        $('#governor').html('<option value="">Select Governor</option');
                        $.each(result, function(key, value){
                            $('#governor').append('<option value="'+value.aspiringfullnames+'">'+value.aspiringfullnames+'</option>');  
                        });
                 
 
                     }
                        
            });
        });

        $('#county').change(function (e) { 
            e.preventDefault();
            var county=$(this).val();
            
            
            $('#senator').html('');
           
          
            $.ajax({
                        type: "POST",
                        url: "/senator",
                        
                        data:{
                            'county': county,
                          
                        },
                        datatype: "json",
                        success:function(result) {
                           
                            console.log(result);

                    
                        $('#senator').html('<option value="">Select senator</option');
                        $.each(result, function(key, value){
                            $('#senator').append('<option value="'+value.aspiringfullnames+'">'+value.aspiringfullnames+'</option>');  
                        });
                 
 
                     }
                        
            });
        });





        $('#county').change(function (e) { 
            e.preventDefault();
            var county=$(this).val();
            
            
            $('#womenrep').html('');
           
          
            $.ajax({
                        type: "POST",
                        url: "/womenrep",
                        
                        data:{
                            'county': county,
                          
                        },
                        datatype: "json",
                        success:function(result) {
                           
                            console.log(result);

                    
                        $('#womenrep').html('<option value="">Select women rep</option');
                        $.each(result, function(key, value){
                            $('#womenrep').append('<option value="'+value.aspiringfullnames+'">'+value.aspiringfullnames+'</option>');  
                        });
                 
 
                     }
                        
            });
        });


      
        $('#constituency').change(function (e) { 
            e.preventDefault();
            var mp=$(this).val();
            
            
            $('#mp').html('');
           
          
            $.ajax({
                        type: "POST",
                        url: "/mp",
                        
                        data:{
                            'mp': mp,
                          
                        },
                        datatype: "json",
                        success:function(result) {
                           
                            console.log(result);

                    
                        $('#mp').html('<option value="">Select women rep</option');
                        $.each(result, function(key, value){
                            $('#mp').append('<option value="'+value.aspiringfullnames+'">'+value.aspiringfullnames+'</option>');  
                        });
                 
 
                     }
                        
            });
        });
$('#ward').change(function (e) { 
            e.preventDefault();
            var ward=$(this).val();
            
            
            $('#mca').html('');
           
          
            $.ajax({
                        type: "POST",
                        url: "/mca",
                        
                        data:{
                            'ward': ward,
                          
                        },
                        datatype: "json",
                        success:function(result) {
                           
                            console.log(result);

                    
                        $('#mca').html('<option value="">Select women rep</option');
                        $.each(result, function(key, value){
                            $('#mca').append('<option value="'+value.aspiringfullnames+'">'+value.aspiringfullnames+'</option>');  
                        });
                 
 
                     }
                        
            });
        });


        
    });
</script>

@endsection