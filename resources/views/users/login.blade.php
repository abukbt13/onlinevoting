@extends('master')
@extends('layouts.navigation.header_navbar')


@section('title','login')

@section('content')

    <div class="row d-flex justify-content-sm-center mt-5">
        <div class="col col-sm-4">
        <form action="{{url('login')}}" method="post">
            @if(Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif 
            @if(Session::has('fail'))
            <p class="alert alert-success">{{Session::get('fail')}}</p>
            @endif

            @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                <div>{{$error}}</div>
                                @endforeach

                            </div>
                            @endif

            
            @csrf
        <div class="mb-3">
            <label for="" class="form-label">National Id No</label>
            <input type="number" name="uniqueid" value="{{old('uniqueid')}}" class="form-control" id="" aria-describedby="">
            <span class="text text-danger">@error('uniqueid'){{$message}}@enderror</span>

        </div>
    
        <div class="mb-3">
            <label for="" class="form-label">Enter Your secretkey</label>
            <input type="text" name="secretkey" value="{{old('secretkey')}}"  class="form-control" id="">
            <span class="text text-danger">@error('secretkey'){{$message}}@enderror</span>
        </div> 
             
        <button type="submit"  class="btn btn-secondary">Login To Start Voting</button>
        </form>
        </div>
    </div>
    @endsection